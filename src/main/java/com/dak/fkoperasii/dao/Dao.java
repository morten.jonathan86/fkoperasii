package com.dak.fkoperasii.dao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;

/**
 *
 * @author risyamaulana
 */
public class Dao {

    @Resource(name = "sharedEntityManagerInternal")
    protected EntityManager em;
}
