/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;


import com.dak.fkoperasii.entity.MCustomCoa;
import com.dak.fkoperasii.entity.MPresentasePlafon;
import com.dak.fkoperasii.entity.MRateAsuransi;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MPresentasePlafonDao")
@Transactional
public class MPresentasePlafonDao extends Dao{

    
    public MPresentasePlafon getPresentasePlafon() {
        try {
          MPresentasePlafon mp = (MPresentasePlafon) em.createQuery("SELECT mp from MPresentasePlafon mp WHERE mp.status='Y'")
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MRateAsuransi getRateAsuransi(Integer jangka_waktu,String status_pegawai) {
        try {
          MRateAsuransi mra = (MRateAsuransi) em.createQuery("SELECT mra from MRateAsuransi mra WHERE mra.jangka_waktu = :jangka_waktu and mra.status_aktif ='Aktif' and mra.status_pegawai=:status_pegawai")
                    .setParameter("jangka_waktu", jangka_waktu)
                  .setParameter("status_pegawai",status_pegawai)
                    .setMaxResults(1)
                    .getSingleResult();
            return mra;
        } catch (NoResultException nre) {
            return null;
        }
    }


    
    public MCustomCoa getBiayaMaterai() {
        try {
          MCustomCoa mcc = (MCustomCoa) em.createQuery("SELECT mcc from MCustomCoa mcc WHERE mcc.nm_label = 'biaya_materai'")
                    .setMaxResults(1)
                    .getSingleResult();
            return mcc;
        } catch (NoResultException nre) {
            return null;
        }
    }
}
