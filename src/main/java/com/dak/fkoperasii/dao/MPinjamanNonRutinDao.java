/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MPinjamanNonRutin;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author arfandiusemahu
 */
@Repository(value = "MPinjamanNonRutinDao")
@Transactional
public class MPinjamanNonRutinDao extends Dao{
    public List<MPinjamanNonRutin> viewPinjamanNonRutin() {
        try {
            List mpnr = null;
            mpnr = em.createQuery("SELECT mpnr from MPinjamanNonRutin mpnr")
                    .getResultList();
            return mpnr;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Object pinjamanNonRutinByBulanTahun(int bulan, Integer tahun) {
        try {
          return em.createNativeQuery("select jasa from m_conf_pinjaman_nonrutin m where m.tahun=:tahun and :bulan between m.periode_dari and m.periode_sampai")
                    .setParameter("bulan", bulan)
                    .setParameter("tahun", tahun)
        .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
