/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MPinjamanMotor;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author arfandiusemahu
 */
@Repository(value = "MPinjamanMotorDao")
@Transactional
public class MPinjamanMotorDao extends Dao{
    public List<MPinjamanMotor> viewPinjamanMotor() {
        try {
            List mpt = null;
            mpt = em.createQuery("SELECT mpt from MPinjamanMotor mpt")
                    .getResultList();
            return mpt;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Object pinjamanMotorByBulanTahun(int bulan, Integer tahun) {
        try {
          return  em.createNativeQuery("SELECT jasa from m_conf_pinjaman_motor mpt WHERE mpt.tahun = :tahun AND :bulan between mpt.periode_dari and mpt.periode_sampai")
                    .setParameter("bulan", bulan)
                    .setParameter("tahun", tahun)
                    .getSingleResult();

        } catch (NoResultException nre) {
            return null;
        }
    }
}
