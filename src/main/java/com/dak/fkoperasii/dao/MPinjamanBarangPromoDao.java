/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MPinjamanBarangPromo;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author arfandiusemahu
 */
@Repository(value = "MPinjamanBarangPromoDao")
@Transactional
public class MPinjamanBarangPromoDao extends Dao{
    public List<MPinjamanBarangPromo> viewPinjamanBarangPromo() {
        try {
            List mpbp = null;
            mpbp = em.createQuery("SELECT mpbp from MPinjamanBarangPromo mpbp")
                    .getResultList();
            return mpbp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Object pinjamanBarangPromoByBulanTahun(int bulan, Integer tahun) {
        try {
          return em.createNativeQuery("SELECT jasa from m_conf_pinjaman_barang_promo mpbp WHERE mpbp.tahun = :tahun AND :bulan between mpbp.periode_dari and mpbp.periode_sampai")
                    .setParameter("bulan", bulan)
                    .setParameter("tahun", tahun)

                    .getSingleResult();

        } catch (NoResultException nre) {
            return null;
        }
    }
}
