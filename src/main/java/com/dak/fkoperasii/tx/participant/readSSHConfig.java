package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MSetting;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.dak.fkoperasii.utility.Tripledes;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONObject;

import java.io.Serializable;

public class readSSHConfig implements TransactionParticipant {
    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    @Override
    public int prepare(long l, Serializable serializable) {
        Context ctx = (Context) serializable;
        try {
            final MSetting setting = SpringInit.getmSettingDao().getSettingByParam("ssh_setting");
            final Tripledes tdes = new Tripledes(Constant.SETTING.TRIDES_MOB_ENCRYPT);
            final JSONObject jsonSetting = new JSONObject(tdes.decrypt(setting.getValue()));
            MRc rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonSetting);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            ctx.put(Constant.WS.RESPONSE, dr);
        }catch (Exception e){
            e.printStackTrace();
            return PREPARED|NO_JOIN;
        }
        return PREPARED|NO_JOIN;

    }

    @Override
    public void commit(long l, Serializable serializable) {

    }

    @Override
    public void abort(long l, Serializable serializable) {

    }
}
