/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.*;
import com.dak.fkoperasii.spring.SpringInit;


import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.dak.fkoperasii.utility.Tripledes;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Morten Jonathan
 */
public class SavePengajuanDanaAll implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";


    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        Calendar cal = Calendar.getInstance();
        DecimalFormat df = new DecimalFormat("#");

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        Date tgl_tempo;
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);


        try {
            //getDataSSH
            final MSetting setting = SpringInit.getmSettingDao().getSettingByParam("ssh_setting");
            final Tripledes tdes = new Tripledes(Constant.SETTING.TRIDES_MOB_ENCRYPT);
            final JSONObject jsonSetting = new JSONObject(tdes.decrypt(setting.getValue()));
            final String SFTPHOST = jsonSetting.getString("ip");
            final int SFTPPORT = jsonSetting.getInt("port");
            final String SFTPUSER = jsonSetting.getString("user");
            final String SFTPPASS = jsonSetting.getString("password");
            String SFTPWORKINGDIRROOT = jsonSetting.getString("path");

            StringBuilder sb = new StringBuilder();
            log.info("================= STARTING SAVE PENGAJUAN ");

            log.info("================= MASUK TRY ");
            int total = 0;
            String ajuan_id;
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("================= MASUK NASABAH " + nsbh.getAktif());
            Integer jenis = jsonReq.getInt("jenis");
            MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByIdStatus(jenis, nsbh.getStatus_pegawai());
            Integer nominal = jsonReq.getInt("nominal");
            Integer lama_angsuran = jsonReq.getInt("lamaangsuran");
            String ket = jsonReq.getString("keterangan");
            String gapok = jsonReq.getString("penghasilan");
            MultipartFile[] file_images = (MultipartFile[]) ctx.get(Constant.IMAGE);
            MultipartFile[] file_pdf = (MultipartFile[]) ctx.get(Constant.PDF);


            if (nsbh.getAktif().equals("Y")) {
                log.info("================= MASUK IF ");
                nsbh.setGaji_pokok(Integer.parseInt(gapok));
                SpringInit.getmNasabahDao().saveOrUpdate(nsbh);
                log.info("==== BERHASIL UPDATE");

                MPengajuan mj = SpringInit.getMPengajuanDao().pengajuanByAjuanId();
                MPengajuan mp = new MPengajuan();
                int no_aj = 1;
                if (mj != null) no_aj = Integer.valueOf(mj.getNo_ajuan()) + 1;

                mp.setNo_ajuan("" + no_aj);
                log.info("================= NO AJUAN " + mp.getNo_ajuan());

                if (mj != null) {
                    String id_trx = mj.getAjuan_id().substring(11, 13);
                    log.info("======================= GET ID TRANSAKSI " + mj.getAjuan_id());

                    if (Integer.parseInt(id_trx) == 00) {
                        id_trx = mj.getAjuan_id().substring(10, 13);
                        log.info("============== SUBSTRING ID TRX " + id_trx);

                    } else {
                        id_trx = mj.getAjuan_id().substring(10, 13);
                        log.info("============== SUBSTRING ID TRX " + id_trx);

                    }
                    String AN = "" + (Integer.parseInt(id_trx) + 1);
                    log.info("======================= ISI AN " + AN);
                    String Nol = "";
                    if (AN.length() == 1) {
                        Nol = "00";
                    } else if (AN.length() == 2) {
                        Nol = "0";
                    } else if (AN.length() == 3) {
                        Nol = "";
                    }
                    ajuan_id = "PRM.19.08." + Nol + AN;
                } else {
                    ajuan_id = "PRM.19.08.001";
                }

                log.info("======================= ISI AJUAN ID " + ajuan_id);

                Double biaya_adm = (nominal / 100) * mjp.getPersentase_pembahasan();
                log.info("===== BIAYA ADM " + biaya_adm);
                if (biaya_adm < 25000.0)
                    biaya_adm = 25000.0;

                Double waktu = Double.valueOf(lama_angsuran) / 12;
                Double wkt = Math.ceil(waktu);
                Integer k = Integer.parseInt(df.format(wkt.intValue()));
                MRateAsuransi mra = SpringInit.getMPresentasePlafonDao().getRateAsuransi(k, nsbh.getStatus_pegawai());
                MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(jenis);
                MCustomCoa b_materai = SpringInit.getMPresentasePlafonDao().getBiayaMaterai();


                Double premi = nominal * mra.getPersentase_rate() / 100;
                Integer jlh_biaya = Integer.parseInt(df.format(premi)) + mjpinjam.getBiaya_polis() + b_materai.getValue_awal();

                mp.setAjuan_id(ajuan_id);
                mp.setAnggota_id(Integer.parseInt(nsbh.getId().toString()));
                mp.setTgl_input(datetime.format(new Date()));
                mp.setJenis("" + jenis);
                mp.setNominal(nominal.toString());
                mp.setLama_ags(lama_angsuran.toString());
                mp.setKeterangan(ket);
                mp.setStatus("0");
                mp.setAlasan("");
                mp.setTgl_cair(date);
                mp.setTgl_update(datetime.format(new Date()));
                mp.setId_barang(4);
                mp.setId_nilai_jaminan(0);
                mp.setBiaya_tambahan(jlh_biaya);
                mp.setBiaya_admin(biaya_adm.intValue());
                if (jsonReq.has("skema_angsuran_bulan"))
                    mp.setSkema_angsuran_bulan(jsonReq.getString("skema_angsuran_bulan"));
                if (jsonReq.has("skema_angsuran_nominal"))
                    mp.setSkema_angsuran_nominal(jsonReq.getString("skema_angsuran_nominal"));


                log.info("=============================== MASUK LOG DATA");
                JSONObject logData = new JSONObject();
//                    logData.put("noajuan", mp.getNo_ajuan().toString());
                logData.put("Ajuan ID", mp.getAjuan_id());
                MNasabah ns = SpringInit.getmNasabahDao().userById(mp.getAnggota_id().longValue());
                logData.put("Nama Anggota", ns.getNama());
                log.info("=============================== TANGGAL INPUT " + mp.getTgl_input());
                logData.put("Tanggal Input", mp.getTgl_input().toString());
                logData.put("Jenis", mjpinjam.getNama_pinjaman());
                logData.put("Nominal", kursIndonesia.format(Integer.parseInt(mp.getNominal())).toString());
                logData.put("Lama Angsuran", mp.getLama_ags().toString());
                logData.put("Keterangan", mp.getKeterangan());
                MStatusPengajuan msp = SpringInit.getMStatusPengajuanDao().statusById(Integer.parseInt(mp.getStatus()));
                logData.put("Status", msp.getStatus());
//                    logData.put("alasan", mp.getAlasan());
                logData.put("Tanggal Cair", mp.getTgl_cair().toString());
//                    logData.put("tanggalupdate",mp.getTgl_update().toString());

                MProduct brg = SpringInit.getMProductDao().barangById(mp.getId_barang());
                logData.put("Nama Barang", brg.getNm_barang());
//                    logData.put("idjaminan",mp.getId_nilai_jaminan().toString());

                log.info("================= MASUK MAU SAVE ");
                if (jenis == 2 && nsbh.getStatus_pegawai().equals("Pegawai Tetap")) {
                    JSONArray arrSkema = jsonReq.getJSONArray("skema_bayar");
                    final int lengthSkema = arrSkema.length();
                    String skemaBulan = "";
                    String skemaNominal = "";
                    JSONObject skemaObj;
                    for (int a = 0; a < lengthSkema; a++) {
                        skemaObj = arrSkema.getJSONObject(a);
                        skemaBulan = skemaBulan + skemaObj.getString("bulan") + ",";
                        skemaNominal = skemaNominal + skemaObj.getInt("nominal_bayar") + ",";
                    }
                    skemaBulan = skemaBulan.substring(0, skemaBulan.length() - 1);
                    skemaNominal = skemaNominal.substring(0, skemaNominal.length() - 1);
                    mp.setSkema_angsuran_bulan(skemaBulan);
                    mp.setSkema_angsuran_nominal(skemaNominal);
                }
                mp = SpringInit.getMPengajuanDao().saveOrUpdate(mp);


//==== UPLOAD FOTO MULTIPART BEDA SERVER ==================================================================================                    
                if (file_images.length > 0) {

                    int i = 1;
                    final String SFTPWORKINGDIR = SFTPWORKINGDIRROOT+"pengajuan";
//                    String path = "/opt/spring/koperasiDIY/webapps/cms/pengajuan/";
                    String path = "src/main/resources/";
//                    String SFTPHOST = "103.47.210.235";

                    JSch jsch = new JSch();
                    for (MultipartFile filed : file_images) {
                        String filename = filed.getOriginalFilename();
                        String LOCALDIRECTORY = path + filename;
                        File convFile = new File(LOCALDIRECTORY);
                        convFile.createNewFile();
                        FileOutputStream stream = new FileOutputStream(convFile);
                        stream.write(filed.getBytes());
                        stream.flush();
                        stream.close();
                        try {

                            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                            log.info("LEWATTTTTTTTTTTTTTTTT1");

                            session.setPassword(SFTPPASS);
                            log.info("LEWATTTTTTTTTTTTTTTTT2");

                            java.util.Properties config = new java.util.Properties();
                            log.info("LEWATTTTTTTTTTTTTTTTT3");

                            config.put("StrictHostKeyChecking", "no");
                            log.info("LEWATTTTTTTTTTTTTTTTT4");

                            session.setConfig(config);
                            log.info("LEWATTTTTTTTTTTTTTTTT5");

                            session.connect(); // Create SFTP Session
                            log.info("LEWATTTTTTTTTTTTTTTTT6");

                            channel = session.openChannel("sftp"); // Open SFTP Channel
                            log.info("LEWATTTTTTTTTTTTTTTTT7");

                            channel.connect();
                            log.info("LEWATTTTTTTTTTTTTTTTT8");

                            channelSftp = (ChannelSftp) channel;
                            log.info("LEWATTTTTTTTTTTTTTTTT9");

                            channelSftp.cd(SFTPWORKINGDIR); // Change Directory on SFTP Server
                            log.info("LEWATTTTTTTTTTTTTTTTT10");

                            recursiveUpload(LOCALDIRECTORY, SFTPWORKINGDIR);
                            log.info("LEWATTTTTTTTTTTTTTTTT11");
                            deleteFile(filename,path);
//                            channelSftp.chown(1053, SFTPWORKINGDIR +"/"+ filename);

                            MBuktiJaminan mbj = new MBuktiJaminan();
                            MPengajuan mpn = SpringInit.getMPengajuanDao().getIdPengajuan();
                            int id_pengajuan = mpn.getId();
                            log.info("File name : " + filename);
                            mbj.setId_bukti_jaminan(id_pengajuan);
                            mbj.setFoto_bukti(filename);
                            mbj.setNilai_agunan(0);
                            SpringInit.getMBuktiJaminanDao().saveOrUpdate(mbj);

                        } catch (Exception ex) {
                            log.info("MASUK CATCH GAGAL");
                            ex.printStackTrace();
                        } finally {
                            if (channelSftp != null)
                                channelSftp.disconnect();
                            if (channel != null)
                                channel.disconnect();
                            if (session != null)
                                session.disconnect();

                        }
                    }
                }


                if (file_pdf.length > 0) {
                    final String SFTPWORKINGDIR = SFTPWORKINGDIRROOT + "persyaratan";
//                    String path = "/opt/spring/koperasiDIY/webapps/cms/persyaratan/";
                    String path = "src/main/resources/";

                    JSch jsch = new JSch();
                    for (MultipartFile filepdf : file_pdf) {
                        String filename = filepdf.getOriginalFilename();
                        String LOCALDIRECTORY = path + filename;
                        File convFile = new File(LOCALDIRECTORY);
                        convFile.createNewFile();
                        FileOutputStream stream = new FileOutputStream(convFile);
                        stream.write(filepdf.getBytes());
                        stream.flush();
                        stream.close();

                        try {

                            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                            log.info("LEWAT1");

                            session.setPassword(SFTPPASS);
                            log.info("LEWAT2");

                            java.util.Properties config = new java.util.Properties();
                            log.info("LEWAT3");

                            config.put("StrictHostKeyChecking", "no");
                            log.info("LEWAT4");

                            session.setConfig(config);
                            log.info("LEWATT5");

                            session.connect(); // Create SFTP Session
                            log.info("LEWAT6");

                            channel = session.openChannel("sftp"); // Open SFTP Channel
                            log.info("LEWAT7");

                            channel.connect();
                            log.info("LEWAT8");

                            channelSftp = (ChannelSftp) channel;
                            log.info("LEWAT9");

                            channelSftp.cd(SFTPWORKINGDIR); // Change Directory on SFTP Server
                            log.info("LEWAT10");

                            recursiveUpload(LOCALDIRECTORY, SFTPWORKINGDIR);
                            log.info("LEWAT11");
                            deleteFile(filename,path);

//                            channelSftp.chown(1053, SFTPWORKINGDIR +"/"+ filename);




                            MPersyaratanPengajuan mpp = new MPersyaratanPengajuan();
                            MPengajuan mpn = SpringInit.getMPengajuanDao().getIdPengajuan();
                            int id_pengajuan = mpn.getId();
                            log.info("File name : " + filename);
                            mpp.setId_persyaratan(id_pengajuan);
                            mpp.setFile_persyaratan(filename);
                            SpringInit.getMPengajuanDao().saveOrUpdateSyarat(mpp);

                        } catch (Exception ex) {
                            log.info("MASUK CATCH GAGAL");
                            ex.printStackTrace();
                        } finally {
                            if (channelSftp != null)
                                channelSftp.disconnect();
                            if (channel != null)
                                channel.disconnect();
                            if (session != null)
                                session.disconnect();

                        }
                    }
                }

//==== UPLOAD FOTO MULTIPART BEDA SERVER ==================================================================================


                JSONArray array = new JSONArray();
                JSONArray arrays = new JSONArray();

                array.put(logData);
//                    JSONObject json = new JSONObject();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsons = array.getJSONObject(i);
                    JSONArray array2 = jsons.names();
                    for (int j = i; j < array2.length(); j++) {
                        JSONObject logs = new JSONObject();
                        logs.put("item", array2.getString(j));
                        logs.put("value", jsons.get(array2.getString(j)));
                        arrays.put(logs);
                    }
                }

                JSONObject json2 = new JSONObject();
                json2.put("data", arrays);

                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());


            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

    private static void recursiveUpload(String sourcePath, String destinationPath) throws SftpException, FileNotFoundException {

        File sourceFile = new File(sourcePath);
        if (sourceFile.isFile()) {

            channelSftp.cd(destinationPath);
            if (!sourceFile.getName().startsWith("."))
                channelSftp.put(new FileInputStream(sourceFile), sourceFile.getName(), ChannelSftp.OVERWRITE);

        } else {

            System.out.println("inside else " + sourceFile.getName());
            File[] files = sourceFile.listFiles();

            if (files != null && !sourceFile.getName().startsWith(".")) {

                channelSftp.cd(destinationPath);
                SftpATTRS attrs = null;

                try {
                    attrs = channelSftp.stat(destinationPath + "/" + sourceFile.getName());
                } catch (Exception e) {
                    System.out.println(destinationPath + "/" + sourceFile.getName() + " not found");
                }

                // else create a directory
                if (attrs != null) {
                    System.out.println("Directory exists IsDir=" + attrs.isDir());
                } else {
                    System.out.println("Creating dir " + sourceFile.getName());
                    channelSftp.mkdir(sourceFile.getName());
                }

                for (File f : files) {
                    recursiveUpload(f.getAbsolutePath(), destinationPath + "/" + sourceFile.getName());
                }

            }
        }

    }


    private boolean deleteFile(String fileName,String path) {
        try {
            // Provide the path to the local directory where the file is located
            Path filePath = Paths.get(path, fileName);

            // Delete the file
            Files.delete(filePath);

            // If deletion is successful, return true
            return true;
        } catch (IOException e) {
            // Handle the exception (e.g., log it)
            e.printStackTrace();
            return false;
        }
    }
}
