/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.application;

import java.security.KeyFactory;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 1star
 */
public class Test2 {
    
    public static void main(String[] args) throws Exception {
String jsonData="{\n" +
"	\"rc\": \"00\",\n" +
"	\"data\": {\n" +
"		\"nama\": \"HANGKOSO, R\",\n" +
"		\"pinjaman\": [\n" +
"			{\n" +
"				\"sisa_tagihan\": \"Rp. -0\",\n" +
"				\"kode_pinjam\": \"PJ01010\",\n" +
"				\"sisa_angsuran\": \"Rp. -0\",\n" +
"				\"tgl_perjanjian\": \"2019-01-20\",\n" +
"				\"lama_angsuran\": \"36\",\n" +
"				\"angsuran_ke\": 37,\n" +
"				\"jenis_pinjaman\": \"Pinjaman Rutin\",\n" +
"				\"pokok\": \"Rp. 0\",\n" +
"				\"nominal_pinjam\": \"Rp. 50,000,000\",\n" +
"				\"angsuran\": \"Rp. 0\",\n" +
"				\"bunga\": \"Rp. 0\",\n" +
"				\"tgl_tempo\": \"-\"\n" +
"			}\n" +
"		],\n" +
"		\"simpanan_pokok\": \"100000\",\n" +
"		\"saldo\": \"29759399\",\n" +
"		\"simpanan_sukarela\": \"5799399\",\n" +
"		\"username\": \"085156973768\",\n" +
"		\"simpanan_wajib\": \"23860000\"\n" +
"	},\n" +
"	\"rm\": \"Sukses\"\n" +
"}";
    JSONObject obj = new JSONObject(jsonData);
    
    try{

    JSONObject obj1 = obj.getJSONObject("data");
    JSONArray obj2 = obj1.getJSONArray("pinjaman");
    String tgl_perjanjian="error";
    String tgl_tempo="error";
    String tglPerjanjianHasil ="-";
    String tglTempoHasil="-";
    for (int i = 0; i < obj2.length(); i++)
    {
        tgl_perjanjian = obj2.getJSONObject(i).getString("tgl_perjanjian");
        tgl_tempo = obj2.getJSONObject(i).getString("tgl_tempo");
    }
    
    System.out.println("hasil 1 "+obj.toString());
    System.out.println("hasil 2 "+obj1.toString());
    System.out.println("hasil 3 "+obj2.toString());
    System.out.println("hasil 4 "+tgl_perjanjian);
    System.out.println("hasil 5 "+tgl_tempo);
    
    DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-mm");
    DateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy");
    
    Date tglPerjanjian = dateFormat.parse(tgl_perjanjian);
    tglPerjanjianHasil = dateFormat2.format(tglPerjanjian);

    if(!(tgl_tempo.equals("-"))){
    
    Date tglTempo = dateFormat.parse(tgl_tempo);
    tglTempoHasil = dateFormat2.format(tglTempo);
    }


    for (int i = 0; i < obj2.length(); i++)
{
  obj2.getJSONObject(i).put("tgl_perjanjian",tglPerjanjianHasil);
  obj2.getJSONObject(i).put("tgl_tempo",tglTempoHasil);
}
    obj1.put("pinjaman",obj2);
    obj.put("data",obj1);

    System.out.println("hasil 5 "+obj2.toString());
    System.out.println("hasil 6 "+obj1.toString());
    System.out.println("hasil 7 "+obj.toString());
    }catch(Exception e){
    e.printStackTrace();
    System.out.println("ERROR = "+e);

    }
}
}
